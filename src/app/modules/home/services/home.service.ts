import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ICountry } from '../interfaces/country';
import { IState } from '../interfaces/state';

@Injectable()
export class HomeService {
  constructor(private http: HttpClient) {}
  private headers = new HttpHeaders({ 'X-CSCAPI-KEY': environment.apiKey });
  getAllContries() {
    return this.http.get<ICountry[]>(`${environment.countryApi}/countries`, {
      headers: this.headers,
    });
  }
  getAllStates() {
    return this.http.get<IState[]>(`${environment.countryApi}/states`, {
      headers: this.headers,
    });
  }
}

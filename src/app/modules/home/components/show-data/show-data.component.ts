import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IPersonData } from '../../interfaces/personData';

@Component({
  selector: 'show-data',
  templateUrl: './show-data.component.html',
  styleUrls: ['./show-data.component.scss'],
})
export class ShowDataComponent implements OnInit {
  person!: IPersonData;

  constructor(private router: Router) {
    this.router.getCurrentNavigation()?.extras.state;
  }

  ngOnInit() {
    this.person = history.state;
  }

  returnForm() {
    this.router.navigate(['/']);
  }
}

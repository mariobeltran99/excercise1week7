import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { BirthDateValidators } from '../../common/validators/birthDate.validators';
import { ICountry } from '../../interfaces/country';
import { IPersonData } from '../../interfaces/personData';
import { IState } from '../../interfaces/state';
import { HomeService } from '../../services/home.service';

@Component({
  selector: 'form-user',
  templateUrl: './form-user.component.html',
  styleUrls: ['./form-user.component.scss'],
})
export class FormUserComponent implements OnInit {
  constructor(
    private api: HomeService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {}
  formPerson!: FormGroup;
  countries: ICountry[] = [];
  states: IState[] = [];
  subStates: IState[] = [];
  errorMessage: string = '';
  showAlert: boolean = false;
  viewPassword: boolean = false;
  viewConfirmPass: boolean = false;

  ngOnInit() {
    this.initializeForm();
    this.loadData();
  }

  initializeForm() {
    this.formPerson = this.formBuilder.group({
      firstName: new FormControl(null, [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(15),
        Validators.pattern(/^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/),
      ]),
      lastName: new FormControl(null, [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(15),
        Validators.pattern(/^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/),
      ]),
      email: new FormControl(null, [Validators.required, Validators.email]),
      telephoneNumber: new FormControl(null, [
        Validators.required,
        Validators.pattern(/^[6-7,2][0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]$/),
      ]),
      password: new FormControl(null, [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(15),
        Validators.pattern(
          /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/
        ),
      ]),
      confirmPassword: new FormControl(null, [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(15),
      ]),
      birthDate: new FormControl(null, [
        Validators.required,
        BirthDateValidators.validDate,
      ]),
      genre: new FormControl(null, [Validators.required]),
      country: new FormControl(null, [Validators.required]),
      state: new FormControl(null),
      personalSite: new FormControl(null, [
        Validators.pattern(
          /[(http(s)?):\/\/(www\.)?a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/
        ),
      ]),
      aboutMe: new FormControl(null, [Validators.maxLength(250)]),
      isAgree: new FormControl(null, [Validators.requiredTrue]),
    });
  }
  loadData() {
    this.api.getAllContries().subscribe((data) => {
      this.countries = data;
    });
    this.api.getAllStates().subscribe((data) => {
      this.states = data;
    });
  }
  nameField(name: string) {
    return this.formPerson.get(name);
  }
  isFieldValid(field: string) {
    const form = this.formPerson.get(field);
    return (form?.touched && !form.valid) || form?.dirty;
  }
  fieldValid(field: string) {
    const form = this.formPerson.get(field);
    return form?.touched && form.valid && form?.dirty;
  }
  onChangeCountry() {
    this.subStates = [];
    this.subStates = this.states.filter(
      (item) => item.country_code == this.formPerson.get('country')?.value.iso2
    );
  }

  fixDate(date: string): Date {
    const birth = new Date(date);
    birth.setDate(birth.getDate() + 1);
    return birth;
  }
  get yesterdayDate(): string {
    const yesterday = new Date();
    yesterday.setDate(yesterday.getDate() - 1);
    const day = yesterday.getDate();
    const month = yesterday.getMonth() + 1;
    const year = yesterday.getFullYear();
    if (day < 10 && month < 10) {
      return `${year}-0${month}-0${day}`;
    } else if (day < 10 && month >= 10) {
      return `${year}-${month}-0${day}`;
    } else if (day >= 10 && month < 10) {
      return `${year}-0${month}-${day}`;
    } else {
      return `${year}-${month}-${day}`;
    }
  }
  onSubmit() {
    if (this.formPerson.valid) {
      let personData: IPersonData;
      const {
        firstName,
        lastName,
        email,
        password,
        confirmPassword,
        birthDate,
        telephoneNumber,
        personalSite,
        aboutMe,
        genre,
        country,
        state,
        isAgree,
      } = this.formPerson.value;
      if (password == confirmPassword) {
        personData = {
          firstName: firstName,
          lastName: lastName,
          email: email,
          password: password,
          birthDate: this.fixDate(birthDate),
          telephoneNumber: telephoneNumber,
          personalSite: personalSite != null ? personalSite : '',
          aboutMe: aboutMe?.trim() != null ? aboutMe : '',
          genre: genre,
          address: {
            country: country?.name,
            state: state?.name != null ? state.name : '',
          },
          isAgree: isAgree,
        };
        console.log(personData);
        this.cleanForm();
        this.router.navigate(['/show-data'], { state: personData });
      } else {
        this.errorMessage = 'Passwords not match';
        this.showAlert = true;
      }
    }
  }
  closeAlert() {
    this.showAlert = false;
  }
  cleanForm() {
    this.showAlert = false;
    this.viewPassword = false;
    this.viewConfirmPass = false;
    this.errorMessage = '';
    this.subStates = [];
    const form = this.formPerson;
    form.reset();
    Object.keys(form.controls).forEach((key) => {
      form.controls[key].setErrors(null);
    });
  }
}

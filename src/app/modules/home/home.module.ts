import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReactiveFormsModule } from '@angular/forms';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './components/home/home.component';
import { FormUserComponent } from './components/form-user/form-user.component';
import { SharedModule } from '../shared/shared.module';
import { HomeService } from './services/home.service';
import { ShowDataComponent } from './components/show-data/show-data.component';

@NgModule({
  declarations: [HomeComponent, FormUserComponent, ShowDataComponent],
  imports: [CommonModule, HomeRoutingModule, SharedModule, ReactiveFormsModule],
  providers: [HomeService],
})
export class HomeModule {}

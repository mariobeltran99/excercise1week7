import { IAddress } from './address';

export interface IPersonData {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  birthDate: Date;
  telephoneNumber: string;
  personalSite: string;
  aboutMe: string;
  genre: string;
  address: IAddress;
  isAgree: boolean;
}

export interface IAddress {
  country: string;
  state: string;
}

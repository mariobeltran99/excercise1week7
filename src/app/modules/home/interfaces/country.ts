export interface ICountry {
  name: string;
  iso2: string;
}

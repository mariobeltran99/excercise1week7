import { AbstractControl, ValidationErrors } from '@angular/forms';

export class BirthDateValidators {
  static validDate(control: AbstractControl): ValidationErrors | null {
    const birth = new Date(control?.value);
    const yesterday = new Date();
    birth.setDate(birth.getDate() + 1);
    yesterday.setDate(yesterday.getDate() - 1);
    if (birth > yesterday) {
      return { validDate: true };
    }
    return null;
  }
}

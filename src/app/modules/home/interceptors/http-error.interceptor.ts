import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
  constructor(private toast: ToastrService) {}
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      retry(3),
      catchError((error) => this.errorHandler(error))
    );
  }
  errorHandler(error: HttpErrorResponse): Observable<never> {
    let errorMessageConsole = '';
    let errorMessageToast = '';
    if (error instanceof HttpErrorResponse) {
      if (error.error instanceof ErrorEvent) {
        errorMessageToast = error.message;
      } else {
        switch (error.status) {
          case 400:
            errorMessageToast =
              'Bad Request: The server could not interpret the request given invalid syntax';
            break;
          case 401:
            errorMessageToast =
              'Authentication is required to get the requested response';
            break;
          case 403:
            errorMessageToast =
              'The client does not have the necessary permissions for certain content';
            break;
          case 404:
            errorMessageToast =
              'The server could not find the requested content';
            break;
          default:
            errorMessageToast = 'Unregistered client error';
            break;
        }
      }
      errorMessageConsole = `Client Error -> Code: ${error.status} - ${error.message}`;
    } else {
      errorMessageConsole = `Server Error ${
        (error as HttpErrorResponse).status
      }`;
      errorMessageToast = 'Server Error';
    }
    this.toast.error(errorMessageToast, `Error ${error.status}`, {
      timeOut: 5000,
    });
    return throwError(errorMessageConsole);
  }
}

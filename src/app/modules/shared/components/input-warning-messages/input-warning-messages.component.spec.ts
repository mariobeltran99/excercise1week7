import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InputWarningMessagesComponent } from './input-warning-messages.component';

describe('InputWarningMessagesComponent', () => {
  let component: InputWarningMessagesComponent;
  let fixture: ComponentFixture<InputWarningMessagesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InputWarningMessagesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputWarningMessagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

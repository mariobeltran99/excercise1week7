import { Component } from '@angular/core';

@Component({
  selector: 'input-warning-messages',
  templateUrl: './input-warning-messages.component.html',
  styleUrls: ['./input-warning-messages.component.scss'],
})
export class InputWarningMessagesComponent {}

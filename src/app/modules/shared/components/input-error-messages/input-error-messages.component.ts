import { Component } from '@angular/core';

@Component({
  selector: 'input-error-messages',
  templateUrl: './input-error-messages.component.html',
  styleUrls: ['./input-error-messages.component.scss'],
})
export class InputErrorMessagesComponent {}

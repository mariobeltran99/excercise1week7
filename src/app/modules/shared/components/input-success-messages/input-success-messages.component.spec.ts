import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InputSuccessMessagesComponent } from './input-success-messages.component';

describe('InputSuccessMessagesComponent', () => {
  let component: InputSuccessMessagesComponent;
  let fixture: ComponentFixture<InputSuccessMessagesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InputSuccessMessagesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputSuccessMessagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

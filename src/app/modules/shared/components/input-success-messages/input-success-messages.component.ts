import { Component } from '@angular/core';

@Component({
  selector: 'input-success-messages',
  templateUrl: './input-success-messages.component.html',
  styleUrls: ['./input-success-messages.component.scss'],
})
export class InputSuccessMessagesComponent {}

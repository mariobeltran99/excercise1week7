import { Component } from '@angular/core';

@Component({
  selector: 'navbar',
  template: `
    <header class="header">
      <h1 class="logo1">
        <a
          ><i class="fab fa-wpforms fa-2x"></i>
          <span class="spacing"> Personal Form</span></a
        >
      </h1>
      <ul class="main-nav">
        <a class="active-page">Excercise 1 - Week 7</a>
      </ul>
    </header>
  `,
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent {}

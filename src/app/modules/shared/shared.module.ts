import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './components/card/card.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { InputErrorMessagesComponent } from './components/input-error-messages/input-error-messages.component';
import { InputWarningMessagesComponent } from './components/input-warning-messages/input-warning-messages.component';
import { InputSuccessMessagesComponent } from './components/input-success-messages/input-success-messages.component';
import { WarningAlertComponent } from './components/warning-alert/warning-alert.component';

@NgModule({
  declarations: [
    CardComponent,
    NavbarComponent,
    InputErrorMessagesComponent,
    InputWarningMessagesComponent,
    InputSuccessMessagesComponent,
    WarningAlertComponent,
  ],
  imports: [CommonModule],
  exports: [
    CardComponent,
    NavbarComponent,
    InputErrorMessagesComponent,
    InputWarningMessagesComponent,
    InputSuccessMessagesComponent,
    WarningAlertComponent,
  ],
})
export class SharedModule {}

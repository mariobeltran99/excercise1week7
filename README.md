# Week 7 - Excercise 1

## Form 📰

It consists of validating the fields of a form about the personal data of a client using reactive form.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.4.

## Prerequisites 📋

_In order to use the application and modify the code, it is necessary to have the following tools installed:_

- [NodeJS v15.11, NPM 7.x.x - Windows](https://nodejs.org/es/download/)
- [NVM v15.11, NPM 7.x.x - Linux](https://github.com/nvm-sh/nvm)
- [Angular v12.2.4](https://github.com/angular/angular-cli)

## Installation Guide 🖥️

_Make sure you have the requested prerequisites installed._

Run the command to install Angular CLI.

```bash
npm install -g @angular/cli
```

Run the command to clone the project.

```bash
git clone https://gitlab.com/mariobeltran99/excercise1week7.git
```

Run the command to install dependencies

```bash
npm install
```

Run the command to execute app

```bash
ng serve
```

And that's all for it to work

## Development server :wrench:

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Deploy :rocket:

It is running in the following link

APP

- <https://reactive-forms-user.vercel.app/>

## Use External API :alien:

API - Country State City API

- <https://countrystatecity.in>

## Creator 👨‍💻

**Mario Josué Beltrán**

- <https://gitlab.com/mariobeltran99>
